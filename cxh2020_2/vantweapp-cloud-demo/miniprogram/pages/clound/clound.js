// pages/clound/clound.js
const db = wx.cloud.database()//初始化数据库
Page({

  /**
   * 页面的初始数据
   */
  data: {
    images:[]
  },
  insert : function(){
    // db.collection('user').add({
    //   data :{
    //     name: 'jerry',
    //     age : 20
    //   },
    //   success: res => {
    //     console.log(res);
    //   },
    //   fail :err =>{
    //     console.log(err);
    //   }
    // })
    db.collection('user').add({
      data :{
        name: 'jack',
        age: 18
      }
    }).then(res=>{
      console.log(res)
    }).catch(err=>{
      console.log(err)
    })
  },
  update: function(){
    db.collection('user').doc('d782d4875f6738990020e02e56e5590c').update({
      data:{
        age: 21
      }
    }).then(res=> {
      console.log(res);
    }).catch(err=> {
      console.log(err);
    })
  },

  select: function(){
    db.collection('user').where({
      name : 'jerry'
    }).get().then(res=>{
      console.log(res);
    }).catch(err=>{
      console.log(err);
    })
  },
  delete: function(){
    db.collection('user').doc('d782d4875f6738990020e02e56e5590c')
    .remove()
    .then(res=>{
      console.log(res);
    }).catch(err=>{
      console.log(err);
    })
  },
  sum: function(){
    wx.cloud.callFunction({
      name: 'sum',
      data: {
        a: 10,
        b: 20
      }
    }).then(res=>{
      console.log(res);
    }).catch(err=>{
      console.log(err);
    })
  },
  getOpenid: function(){
    wx.cloud.callFunction({
      name: 'login'
    }).then(res=> {
      console.log(res)
    }).catch(err=> {
      console.log(err)
    })
    
  },

  batchDelete: function(){
    wx.cloud.callFunction({
      name: 'batchdata'
    }).then(res=> {
      console.log(res)
    }).catch(err=> {
      console.log(err)
    })
  },
  upload: function(){
    // 让用户选择一张图片
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success (res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths);
        // 将图片上传至云存储空间
      wx.cloud.uploadFile({
      // 指定上传到的云路径
      cloudPath: new Date().getTime() +'.png',
      // 指定要上传的文件的小程序临时文件路径
      filePath: tempFilePaths[0],
      // 成功回调
      success: res => {
        //返回文件 ID
        console.log('上传成功', res.fileID)
        db.collection('image').add({
          data: {
            fileID: res.fileID
          }
          }).then(res=>{
            console.log(res);
          }).catch(err=>{
            console.log(res);
        })
      },
      fail: console.error
    })
      }
    })
  },
  getFile: function(){
      wx.cloud.callFunction({
        name:'login',
      }).then(res=>{
        db.collection('image').where({
          _openid: res.result.openid
        }).get().then(res2=>{
          console.log(res2);
          this.setData({
            images: res2.data
          })
        })
      })
  },

  downloadFile: function(envet){
    wx.cloud.downloadFile({
     fileID : envet.target.dataset.fileid, //文件ID
     success: res =>{
      
      //返回临时文件路径
      console.log(res.tempFilePath);
      //保存图片到手机相册
      wx.saveImageToPhotosAlbum({
        filePath: res.tempFilePath,
        success(res) { 
          wx.showToast({
            title:'保存成功'
          })
        }
      })
     },
     fail: console.error
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})